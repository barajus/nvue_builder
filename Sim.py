
#!/usr/bin/env python3
import yaml, os, time, argparse
import json

from air_sdk import AirApi
from colorama import Fore
from Router import *

class Sim:
	def __init__(self,config,topo,data,verbose=False):
		self.config		=	config
		self.topo		= 	topo
		self.verbose	=	verbose
		self.cfg        =   data
		self.check_conf_file()

	def check_conf_file(self):
		with open(str(self.config), "r") as ymlfile:
			self.content = yaml.load(ymlfile,Loader=yaml.FullLoader)
			self.handle_verbose("YAML: " + str(self.content))

	def msg_err(self,msg):
		print("[" + Fore.RED + "NOK" + Fore.RESET + "] - " + msg)
		return msg

	def msg_ok(self,msg):
		print("[" + Fore.GREEN + "OK" + Fore.RESET + "] - " + msg)
		return msg

	def msg_try(self,msg):
		print("[" + Fore.YELLOW + "X" + Fore.RESET + "] - " + msg)
		return msg

	def handle_verbose(self,msg):
		if self.verbose is True:
			print("[" + Fore.BLUE + "X" + Fore.RESET + "] - " + msg)

	# Find if $device is found in list mlag/mh and return RACK
	def lookup_rack(self,name,flavor):
		self.handle_verbose("function lookup")
		# iterate on list in mlag/mh
		for rack in self.content[flavor]:
			# iterate on leave machines
			for leaf in self.content[flavor][rack]["leaves"]:
				self.handle_verbose("iterate sur leaf: " + leaf)
				if leaf == name:
					self.handle_verbose("* Found")
					return rack
				else:
					self.handle_verbose("* Not found")

	def seperator(sef):
		print("===================================================================================")

	# generate conf for a specific device or complete list
	def gen_conf(self,device=None,save=False):
		self.handle_verbose("device: " + str(device))
		if device is None:
			###############################################################
			##################### FULL generation #########################
			###############################################################

			self.handle_verbose("Generate FULL environement")
			for spine in self.content["spines"]:
				self.handle_verbose("spines: " + spine)
				gen_spine(spine,self.content["spines"][spine],self.verbose,save)
				self.seperator()

			for fw in self.content["fw"]:
				self.handle_verbose("fw: " + fw)
				gen_fw(fw,self.content["fw"][fw],self.verbose,save)
				self.seperator()
			
			for border in self.content["borders"]:
				self.handle_verbose("border")
				gen_spine(border,self.content["borders"][border],self.verbose,save)
				self.seperator()

			for rack in self.cfg["leaves_mlag"]:
				for leaf in self.cfg["leaves_mlag"][rack]["leaves"]:
					self.handle_verbose("leaves_mlag")
					#print("rack: " + str(rack) + "leaf: " + str(leaf))
					gen_leaf_mlag(leaf,self.content["leaves_mlag"],rack,self.verbose,save)
					self.seperator()

			for rack in self.cfg["leaves_mh"]:
				for leaf in self.cfg["leaves_mh"][rack]["leaves"]:
					self.handle_verbose("leaves_mh")
					#print("rack: " + str(rack) + " leaf: " + str(leaf))
					gen_leaf_mh(leaf,self.content["leaves_mh"],rack,self.verbose,save)
					self.seperator()
		else:
			###############################################################
			##################### Per DEVICE generation #########################
			###############################################################
			self.handle_verbose("gen conf for device " + device)
			if device in self.content["spines"]:
				gen_spine(device,self.content["spines"][device],self.verbose,save)

			if device in self.content["fw"]:
				gen_fw(device,self.content["fw"][device],self.verbose,save)
			
			elif device in self.content["borders"]:
				gen_border(device,self.content["borders"][device],self.verbose,save)

			elif self.lookup_rack(device,"leaves_mlag") is not None:
				rack = self.lookup_rack(device,"leaves_mlag")
				gen_leaf_mlag(device,self.content["leaves_mlag"],rack,self.verbose,save)

			elif self.lookup_rack(device,"leaves_mh") is not None:
				rack = self.lookup_rack(device,"leaves_mh")
				gen_leaf_mh(device,self.content["leaves_mh"],rack,self.verbose,save)

			else:
				self.msg_err("Category not found")

	def clean_id(self,num):
		num = list(num.split(" "))
		length = len(num)
		num = num[length-1]
		num = num[:-1]
		return num

	def gen_topo(self,reset=False):
		self.handle_verbose("Create topology in Air")
	
		if os.path.isfile("token") is False:
			self.msg_error("Token not found")
			exit()
		else:
			self.handle_verbose("Token found")
    
		self.msg_try("connection to air")
		user = 'jdhaille@nvidia.com'
 
		with open('token', 'r') as file:
			api_token = file.read().rstrip()
  
		air = AirApi(username=user, password=api_token)
		if air.token:
			self.msg_ok("Connection successful")
  		
		# fixme
		name = self.cfg["simulation"]
		self.name = name

		found = False
		for i in air.simulations.list():
			self.handle_verbose("Value: " + str(i))

			if name in str(i):
				found = True
				self.msg_ok("Simulation found: " + str(i))
				sim_id = self.clean_id(str(i)) 
				sim = air.simulations.get(sim_id)
				sim = air.simulations.get(sim_id)
				if (sim.state == "LOADED"):
					self.msg_ok(sim_id + " is LOADED")
					#print(air.services.list)
					#print(air.services.get({sim_id}))
				else:
					self.msg_err("Simulation not loaded " + str({sim.state}))
					if (sim.state == "STORED"):
						sim.start()
						while(sim.state != "LOADED"):
							time.sleep(1)
							sim.refresh()
							print("Loading...")
						print("loaded!")
				
				if reset is True:
					sim_target = air.simulations.get(sim_id)
					self.msg_try("Rebuild")
					sim_target.delete()
					self.gen_topo()

		if found is False:
			self.create_sim(air)
					
	def ztp(self,node):
		# ZTP for mgmt
		if node.name == "oob-mgmt-server":
			self.msg_try("ZTP on oob-mgmt-server")

			commands = []
			commands.append("passwd -x 99999 ubuntu")
			commands.append("echo 'ubuntu:juliendhaille' | chpasswd")
			commands.append("echo ")

			for cmd in commands:
				node.create_instructions(data=cmd, executor='shell')
		
		elif "server" not in node.name:
			self.msg_try("ZTP on " + str(node.name))
			with open('test.ztp') as f:
				for line in enumerate(f):
					node.create_instructions(data=line[1], executor='shell')
			f.close()

	def deploy_nvue(self,node):
		self.msg_try("Pushing NVUE on " + str(node.name))
		with open('generated/'+ node.name + '.conf') as file:
			node.create_instructions(data="touch /tmp/config", executor='shell')
			for line in file:
				cmd = "echo " + str(line.rstrip()) + ">> /tmp/config"
				node.create_instructions(data=cmd, executor='shell')
				#print("Reading conf..." + line.rstrip())
		node.create_instructions(data="bash /tmp/config", executor='shell')
		node.create_instructions(data="nv config apply -y", executor='shell')
		self.msg_ok("Config installed on " + str(node.name))
			
	def create_sim(self,air):
		self.msg_try("Simulation " + str(self.name) + " not found, build in process..")
		topology = air.topologies.create(dot=self.name + '.dot')
		simulation = air.simulations.create(topology=topology, title=self.name)
		simulation.start()
		simulation.refresh()
		self.msg_ok("Simulation created - " + simulation.id)
		
		air.services.create(name="oob-mgmt-server SSH", interface="oob-mgmt-server:eth0", simulation=simulation, dest_port=22)
		self.msg_ok("SSH created")
		i = 0
		while(simulation.state == 'LOADING'):
			i = i + 1
			if i % 5 == 0:
				self.msg_try("Simulation loading " + str(i) + "s")
			time.sleep(1)
			simulation.refresh()

		if (simulation.state == 'LOADED'):
			self.msg_ok("Simulation started (took " + str(i) + "s)")
			self.nodes = air.simulation_nodes.list(simulation=simulation.id)

			begin = time.time()

			list_nodes = []
			list_mgmt = []
			
			for i in self.nodes:
				if i.name == "oob-mgmt-server" or i.name =="oob-mgmt-switch":
					list_mgmt.append(i)
				elif "server" in i.name:
					print("on skip " + i.name)
					continue
				else:
					list_nodes.append(i)



			print("========== ztp for mgmt")
			for i in list_mgmt:
				if i.name == "oob-mgmt-switch":
					continue

				self.ztp(i)

				# configure ansible here
				#frr_contents = '<frr_conf_config_here>'
				#post_cmd = 'systemctl restart frr'
				#data = {'/etc/frr/frr.conf': frr_contents, 'post_cmd':post_cmd}
				#sample_node.create_instructions(data=json.dumps(data), executor='file')

				# commands on mgmt-server
				i.create_instructions(data="mkdir /tmp/toto ; touch /tmp/toto/inv", executor='shell')
				
				#i.create_instructions(data="echo '[all]\nleaf01\nleaf02\n\n[all:vars]\nansible_user=cumulus > /tmp/toto/inv'", executor='shell')
				## i.create_instructions(data="echo '[all]\nleaf01\n' > /tmp/toto/inv", executor='shell')
				
				frr_contents = "[all]\nleaf01\nleaf02\n\n[all:vars]\nansible_user=cumulus\n"
				data = {'/tmp/toto/inv': frr_contents}
				i.create_instructions(data=json.dumps(data), executor='file')



				#i.create_instructions(data='echo '[all]\nleaf01\nleaf02\n\n[all:vars]\nansible_user=cumulus\nansible_ssh_common_args='-o StrictHostKeyChecking=no'\n' > /tmp/toto/inv', executor='shell')


				for node in list_nodes:
					eni_contents = open('generated/'+ node.name + '.conf', 'r').read()
					data = {'/tmp/toto/' + node.name: eni_contents}
					i.create_instructions(data=json.dumps(data), executor='file')
					print("write conf to mgmt server")

					print("=========")
					merde = open('generated/'+ node.name + '.conf', 'r').read()
					data = {'/tmp/' + node.name: merde}
					node.create_instructions(data=json.dumps(data), executor='file')
					print("write conf to node")
					node.create_instructions(data="bash /tmp/"+node.name, executor='shell')
					print("execute ")








				

				cmda="ansible -i /tmp/toto/inv all -m copy -a 'src=/tmp/toto/{{ inventory_hostname }} dest=/tmp/{{ inventory_hostname }}.txt' -m shell -a 'bash /tmp/{{ inventory_hostname }}.txt ; nv config apply -y'"
				cmda="uname -a"
				i.create_instructions(data=cmda, executor='shell')

			
			print("========== ztp for nodes")						
			for i in list_nodes:
				self.ztp(i)

			
			
		
			"""
			print("========== NVUE for nodes")
			for i in list_nodes:
				self.deploy_nvue(i)
			"""
				

			print("Total time:", time.time() - begin)

			id_service = self.clean_id(str(simulation.services))
			id_service = id_service[:-1]
			zizi = air.services.get(id_service)
			print(f'ssh -p {zizi.src_port} {zizi.os_default_username}@{zizi.host}')

		
		 
		
