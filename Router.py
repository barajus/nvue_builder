#!/usr/bin/env python3
confdir = "generated"

def verb(msg,v=False):
  if v is True:
    print("*func " + str(msg))

##################################################
######        Generate type of device       ###### 
##################################################



def gen_fw(device,content,verbose,save=False):
  verb(content,verbose)
  if save is False: 
    feature_hostname(device)
    feature_lo(content["lo"])
  else:
    f = open(confdir + "/" + device + ".conf", "w")
    f.write(feature_hostname(device))
    f.write(feature_lo(content["lo"]))
    f.close()


def gen_spine(device,content,verbose,save=False):
  verb(content,verbose)
  if save is False: 
    feature_hostname(device)
    feature_lo(content["lo"])
    feature_bgp(content["lo"],content["asn"],content["interfaces"])
  else:
    f = open(confdir + "/" + device + ".conf", "w")
    f.write(feature_hostname(device))
    f.write(feature_lo(content["lo"]))
    f.write(feature_bgp(content["lo"],content["asn"],content["interfaces"]))
    f.close()

def gen_leaf_mlag(device,content,rack,verbose,save=False):
  verb(content,verbose)
  if save is False:
    feature_hostname(device)
    feature_lo(content[rack]["leaves"][device]["lo"])
    feature_mlag(content[rack]["leaves"][device]["backup"],content[rack]["mac"],content[rack]["peerlink"],content[rack]["anycast"])
    feature_bgp(content[rack]["leaves"][device]["lo"],content[rack]["leaves"][device]["asn"],content[rack]["interfaces"])
    feature_vxlan(content[rack]["vlan"],content[rack]["leaves"][device]["lo"],content[rack]["vrf"],content[rack]["leaves"][device]["lo"])
    feature_bonding(content[rack]["vlan"],content[rack]["leaves"][device]["ip"],content[rack]["bonds"],content[rack]["vrf"])
  else:
    f = open(confdir + "/" + device + ".conf", "w")
    f.write(feature_hostname(device))
    f.write(feature_lo(content[rack]["leaves"][device]["lo"]))
    f.write(feature_mlag(content[rack]["leaves"][device]["backup"],content[rack]["mac"],content[rack]["peerlink"],content[rack]["anycast"]))
    f.write(feature_bgp(content[rack]["leaves"][device]["lo"],content[rack]["leaves"][device]["asn"],content[rack]["interfaces"]))
    f.write(feature_vxlan(content[rack]["vlan"],content[rack]["leaves"][device]["lo"],content[rack]["vrf"],content[rack]["leaves"][device]["lo"]))
    f.write(feature_bonding(content[rack]["vlan"],content[rack]["leaves"][device]["ip"],content[rack]["bonds"],content[rack]["vrf"]))
    f.close()

def gen_border(device,content,verbose,write=False):
  verb(content,verbose)
  if write is False: 
    feature_hostname(device)
    feature_lo(content["lo"])
    feature_bgp(content["lo"],content["asn"],content["interfaces"])
  else:
    f = open(confdir + "/" + device + ".conf", "w")
    f.write(feature_hostname(device))
    f.write(feature_lo(content["lo"]))
    f.write(feature_bgp(content["lo"],content["asn"],content["interfaces"]))
    f.close()

def gen_leaf_mh(device,content,rack,verbose,write=False):
  verb(content,verbose)
  if write is False:
    feature_hostname(device)
    feature_lo(content[rack]["leaves"][device]["lo"])
    feature_bgp(content[rack]["leaves"][device]["lo"],content[rack]["leaves"][device]["asn"],content[rack]["interfaces"])
    feature_vxlan(content[rack]["vlan"],content[rack]["leaves"][device]["lo"],content[rack]["vrf"],content[rack]["leaves"][device]["lo"])
    feature_mh(content[rack]["interfaces"],content[rack]["bonds"],content[rack]["mac"],content[rack]["leaves"][device]["prio"])
  else:
    f = open(confdir + "/" + device + ".conf", "w")
    f.write(feature_hostname(device))
    f.write(feature_lo(content[rack]["leaves"][device]["lo"]))
    f.write(feature_bgp(content[rack]["leaves"][device]["lo"],content[rack]["leaves"][device]["asn"],content[rack]["interfaces"]))
    f.write(feature_vxlan(content[rack]["vlan"],content[rack]["leaves"][device]["lo"],content[rack]["vrf"],content[rack]["leaves"][device]["lo"]))
    f.write(feature_mh(content[rack]["interfaces"],content[rack]["bonds"],content[rack]["mac"],content[rack]["leaves"][device]["prio"]))
    f.close()



################################################
######        Features definitions        ###### 
################################################

def feature_hostname(device):
  msg = str("nv set system hostname " + str(device) + "\n")
  print(msg)
  return msg

def feature_lo(lo):
  msg = str("nv set interface lo ip address " + str(lo) + "/32\n")
  print(msg)
  return msg

def feature_mlag(eth0,mac,peerlink,anycast):
  msg = "nv set mlag backup " + str(eth0) + " vrf mgmt" + "\n"
  msg = msg + "nv set mlag mac-address " + str(mac) + "\n"
  msg = msg + "nv set interface peerlink bond member " + str(peerlink) + "\n"
  msg = msg + "nv set mlag peer-ip linklocal" + "\n"
  msg = msg + "nv set mlag init-delay 2" + "\n"
  msg = msg + "nv set nve vxlan mlag shared-address " + str(anycast) + "\n"
  msg = msg + "nv set system global anycast-mac " + str(mac) + "\n"
  msg = str(msg)
  print(msg)
  return msg

def feature_mh(interfaces,bonds,mac,prio):
  msg = ""
  if type(interfaces) == list:
    for interface in interfaces:
     msg = msg + "nv set interface " + str(interface) + " evpn multihoming uplink on\n"
  else:
    msg = msg + "nv set interface " + str(interfaces) + " evpn multihoming uplink on\n"
  for bond in bonds:
    msg = msg + "nv set interface " + str(bond) + " evpn multihoming segment local-id " + str(bonds[bond]['id']) + "\n"
    msg = msg + "nv set interface " + str(bond) + " evpn multihoming segment mac-address " + str(mac) + "\n"
    msg = msg + "nv set interface " + str(bond) + " evpn multihoming segment df-preference " + str(prio) + "\n"
  msg = str(msg)
  print(msg)
  return msg

def feature_bgp(lo,asn,interfaces):
  msg = "nv set router bgp router-id " + str(lo) + "\n"
  msg = msg + "nv set router bgp autonomous-system " + str(asn) + "\n"
  msg = msg + "nv set vrf default router bgp address-family ipv4-unicast redistribute connected" + "\n"
  msg = msg + "nv set vrf default router bgp path-selection multipath aspath-ignore on" + "\n"
  if type(interfaces) == list:
    for swp in interfaces:
      msg = msg + "nv set vrf default router bgp neighbor " + str(swp) + " remote-as external" + "\n"
      msg = msg + "nv set vrf default router bgp neighbor " + str(swp) + " address-family l2vpn-evpn" + "\n"
  else:
      msg = msg + "nv set vrf default router bgp neighbor " + str(interfaces) + " remote-as external" + "\n"
      msg = msg + "nv set vrf default router bgp neighbor " + str(interfaces) + " address-family l2vpn-evpn" + "\n"
  print(str(msg))
  return msg

def feature_vxlan(vlan,vxlan,vrf,lo):
  msg = ""
  for vlan_id in vlan:
    msg = msg + "nv set bridge domain br_default vlan " + str(vlan_id) + " vni auto" + "\n"
  msg = msg + "nv set evpn enable on" + "\n"
  msg = msg + "nv set nve vxlan source address " + str(lo) + "\n"
  for tenant in vrf:
    msg = msg + "nv set vrf " + str(tenant) + " evpn vni " + str(vrf[tenant]['l3vni']) + "\n"
  print(str(msg))
  return msg

def feature_bonding(vlan,ip,bonds,vrfs):
  msg = "nv set interface swp1-3,swp51 link state up" + "\n"
  for bond in bonds:
    msg = msg + "nv set interface " + str(bond) + " bond member " + str(bonds[bond]['int']) + "\n"
  for vlan_id in vlan:
      msg = msg + "nv set interface " + str(vlan[vlan_id]['access']) + " bond mlag id " + str(vlan[vlan_id]['access'][-1]) + "\n"
      msg = msg + "nv set interface " + str(vlan[vlan_id]['access']) + " bridge domain br_default access " + str(vlan_id) + "\n"
      msg = msg + "nv set interface vlan" + str(vlan_id) + " vlan " + str(vlan_id) + "\n"
      msg = msg + "nv set interface vlan" + str(vlan_id) + " ip address "+ str(vlan[vlan_id]['sub']) + "." + str(ip) + "/24" + "\n"
      msg = msg + "nv set interface vlan" + str(vlan_id) + " ip vrr address " + str(vlan[vlan_id]['sub']) + ".1/24" + "\n"
      for vrf in vrfs:
        msg = msg + "nv set interface vlan" + str(vlan_id) + " ip vrf " + str(vrf) + "\n"
  print(str(msg))
  return msg