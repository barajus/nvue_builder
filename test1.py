#!/usr/bin/env python3
import argparse, yaml

from air_sdk import AirApi
from colorama import Fore
from Sim import *
from Router import *
		
parser = argparse.ArgumentParser()
parser.add_argument("-c", "--Config", help = "Specify a config file")
parser.add_argument("-a", help="Create topology in AIR",action="store_true")
parser.add_argument("-d", "--Device", help = "Specify a device")
parser.add_argument("-w", help="Export",action="store_true")
parser.add_argument("-v", help="Export",action="store_true")
parser.add_argument("-r", help="Reset",action="store_true")
args = parser.parse_args()


if __name__ == '__main__':

	if not args.Config:
		print("Arg config not found")
		exit()
	else:
		with open(str(args.Config), "r") as ymlfile:
			data = yaml.load(ymlfile,Loader=yaml.FullLoader)

	# Define if we generate a specific device or all of them
	if args.Device:
		device = args.Device
	else:
		device = None

	# Verbose
	if args.v:
		Verbose = True
	else:
		Verbose = False

	# Writing config files
	if args.w:
		save = True
	else:
		save = False

	Simu = Sim(args.Config,"topo",data,Verbose)
	# if no device is given, generate all the configs
	Simu.gen_conf(device,save)

	if args.a:
		if args.r:
			# Rebuild if flag is on
			Simu.gen_topo(True)
		else:
			Simu.gen_topo(False)


