# Instructions
## Motivation
Sometimes during a PoC, customers are not yet comfortable with NVUE CLI and require a live session to execute basic configuration.
This script is a basic parser which generates NVUE commands based on a YAML object model, it can be used from your laptop or even from oob-mgmt-server.
For a real automation workflow, please look at a proper tool, such as the NVUE Ansible module, golden turtle, etc..

The goal is simple:
- Get a quick and customized CLI snippet
- Provide an alternative to the missing `net show example`
- Obviate browsing through documentation while configuring a lab
- Remove the need to maintain notes/snippets/draft for live demo configs

## Instructions
- Clone the repo `git clone https://gitlab.com/barajus/nvue_builder`
- Python3 and PyYAML module are required: `pip3 install PyYAML`
- Create a `yaml` file, or just adjust `example.yaml` as a first try
- Execute `./builder.py -c example.yaml`
- See `./builder.py -h` for options

## Examples
- Generate the full configuration: `./builder.py -c example.yaml`
- Limit to a specific device: `./builder.py -c example.yaml -d leaf01`

