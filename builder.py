#!/usr/bin/env python3
import yaml
import time
import argparse
import os
from air_sdk import AirApi
from colorama import Fore

confdir = "generated"

def clean_id(num):
  num = list(num.split(" "))
  length = len(num)
  num = num[length-1]
  num = num[:-1]
  return num

def msg_error(msg):
  print("[" + Fore.RED + "NOK" + Fore.RESET + "] - " + msg) 
  return msg

def msg_good(msg):
  print("[" + Fore.GREEN + "OK" + Fore.RESET + "] - " + msg) 
  return msg

def msg_try(msg):
  print("[" + Fore.YELLOW + "X" + Fore.RESET + "] - " + msg) 
  return msg

def trigger_air():
  if os.path.isfile("token") is False:
    msg_error("Token not found")
    exit()
  else:
    msg_good("Token found")
    
  msg_try("Connecting to AIR...")
  user = 'jdhaille@nvidia.com'
 
  with open('token', 'r') as file:
    api_token = file.read().rstrip()
  
  air = AirApi(username=user, password=api_token)
  if air.token:
    msg_good("Connection successful")
  
  name = cfg["simulation"]
  
  found = False
  for i in air.simulations.list():
    #print(f"i: {i}")
    if name in str(i):
      found = True
      print(f"[OK] - Simulation found: {i}")

      sim_id = clean_id(str(i)) 
      
      print(f"[OK] - ID found: {sim_id}")
      sim = air.simulations.get(sim_id)
      if (sim.state == "LOADED"):
        print(f"[OK] - {sim_id} loaded")
        print(dir(sim))
        print(air.services.list)
        #print(air.services.get({sim_id}))
      else:
        print(f"[NOK] - Simulation not loaded: {sim.state}")
        if (sim.state == "STORED"):
          sim.start()
          while(sim.state != "LOADED"):
            time.sleep(1)
            sim.refresh()
            print("Loading...")
          print("loaded!")


      if args.r:
        print("Deleted!")
        sim_target = air.simulations.get(sim_id)
        sim_target.delete()
        # rebuild
        trigger_air()
      #print(dir(merde))
  
  if found is False:
    print(f"[NOK] - Simulation '{name}' not found, crafting the topology")
  
    ### Create sim
  
    topology = air.topologies.create(dot=name + '.dot')
    simulation = air.simulations.create(topology=topology, title=name)
    print("Simulation created at:",simulation.id)
    simulation.start()
    simulation.refresh()
    #time.sleep(1)
    # print(dir(simulation))
    # creating SSH
    air.services.create(name="oob-mgmt-server SSH", interface="oob-mgmt-server:eth0", simulation=simulation, dest_port=22)
    print("[OK] - SSH created")
    while(simulation.state == 'LOADING'):
      print("Simulation loading...")
      simulation.refresh()
      time.sleep(1)
    if (simulation.state == 'LOADED'):
      print("started!")

      # ztp routine
      simnodes = air.simulation_nodes.list(simulation=simulation.id)
      for simnode in simnodes:
        commands = []
        # ztp for MGMT
        if simnode.name == "oob-mgmt-server":
          print("[OK] - ZTP oob-mgmt-server!!!")
          commands.append("passwd -x 99999 ubuntu")
          commands.append("echo 'ubuntu:juliendhaille' | chpasswd")
          for cmd in commands:
            simnode.create_instructions(data=cmd, executor='shell')
        # ztp for cumulus nodes
        else:
          print(f"[OK] - ZTP on {simnode.name}")
          commands.append("passwd -x 99999 cumulus")
          commands.append("echo 'cumulus:cumulus' | chpasswd")
          commands.append("echo 'cumulus ALL=(ALL) NOPASSWD:ALL' > /etc/sudoers.d/10_cumulus")
          commands.append("mkdir -p /home/cumulus/.ssh")
          commands.append("cd /home/cumulus/.ssh && wget http://192.168.200.1/authorized_keys")
          commands.append("sleep 2 ; cd /home/ubuntu ; git clone https://barajus:glpat-Z28_b-8fzoxcqat2rAsP@gitlab.com/nvidia-networking/ethernet_ps/clv5_automation")

          for i in commands:
            # print("exec: " + i)
            simnode.create_instructions(data=i, executor='shell')
          print(f"[OK] - ZTP over for {simnode.name}")

          # push conf
          #with open(confdir + "/" + simnode.name + ".conf") as file:
          #  for line in file:
          #    simnode.create_instructions(data=line, executor='shell')
          #    # print("Reading conf..." + line.rstrip())
          #simnode.create_instructions(data="nv config apply -y", executor='shell')
          #print(f"[OK] - Config installed on {simnode.name}")

      # Print SSH
      lala = clean_id(str(simulation.services))
      lala = lala[:-1]
      print("id serv:" + lala)
      print(air.services.get(lala))
      zizi = air.services.get(lala)
      print(f'ssh -p {zizi.src_port} {zizi.os_default_username}@{zizi.host}') 
      


############### functions per type ############### 
def gen_spine(device_name):
  if args.w:
    f = open(confdir + "/" + device_name + ".conf", "w")
    f.write(set_hostdevice_name(device_name))
    f.write(bgp(cfg["spines"][device_name]["lo"],cfg["spines"][device_name]["asn"],cfg["spines"][device_name]["interfaces"]))
    f.close()
    print("[x] - Config exported")
  else:
    set_hostdevice_name(device_name)
    bgp(cfg["spines"][device_name]["lo"],cfg["spines"][device_name]["asn"],cfg["spines"][device_name]["interfaces"])

def gen_border(device_name):
  if args.w:
    f = open(confdir + "/" + device_name + ".conf", "w")
    f.write(set_hostdevice_name(device_name))
    f.write((bgp(cfg["borders"][device_name]["lo"],cfg["borders"][device_name]["asn"],cfg["borders"][device_name]["interfaces"])))
    f.close()
    print("[x] - Config exported")
  else:
    set_hostdevice_name(device_name)
    bgp(cfg["borders"][device_name]["lo"],cfg["borders"][device_name]["asn"],cfg["borders"][device_name]["interfaces"])

def gen_leaf_mlag(device_name,rack):
  if args.w:
    f = open(confdir + "/" + device_name + ".conf", "w")
    f.write(set_hostdevice_name(device_name))
    f.write(mlag( cfg["leaves_mlag"][rack]["leaves"][device_name]["backup"],cfg["leaves_mlag"][rack]["mac"],cfg["leaves_mlag"][rack]["peerlink"],cfg["leaves_mlag"][rack]["anycast"]))
    f.write(bgp(cfg["leaves_mlag"][rack]["leaves"][device_name]["lo"],cfg["leaves_mlag"][rack]["leaves"][device_name]["asn"],cfg["leaves_mlag"][rack]["interfaces"]))
    f.write(vxlan(cfg["leaves_mlag"][rack]["vlan"],cfg["leaves_mlag"][rack]["leaves"][device_name]["lo"],cfg["leaves_mlag"][rack]["vrf"]))
    f.write(bonding(cfg["leaves_mlag"][rack]["vlan"],cfg["leaves_mlag"][rack]["leaves"][device_name]["ip"],cfg["leaves_mlag"][rack]["bonds"],cfg["leaves_mlag"][rack]["vrf"]))
    print("[x] - Config exported")
  else:
    set_hostdevice_name(device_name)
    mlag( cfg["leaves_mlag"][rack]["leaves"][device_name]["backup"],cfg["leaves_mlag"][rack]["mac"],cfg["leaves_mlag"][rack]["peerlink"],cfg["leaves_mlag"][rack]["anycast"])
    bgp(cfg["leaves_mlag"][rack]["leaves"][device_name]["lo"],cfg["leaves_mlag"][rack]["leaves"][device_name]["asn"],cfg["leaves_mlag"][rack]["interfaces"])
    vxlan(cfg["leaves_mlag"][rack]["vlan"],cfg["leaves_mlag"][rack]["leaves"][device_name]["lo"],cfg["leaves_mlag"][rack]["vrf"])
    bonding(cfg["leaves_mlag"][rack]["vlan"],cfg["leaves_mlag"][rack]["leaves"][device_name]["ip"],cfg["leaves_mlag"][rack]["bonds"],cfg["leaves_mlag"][rack]["vrf"])

def gen_leaf_mh(device_name,rack):
  set_hostdevice_name(device_name)
  bgp(cfg["leaves_mh"][rack]["leaves"][device_name]["lo"],
      cfg["leaves_mh"][rack]["leaves"][device_name]["asn"],
      cfg["leaves_mh"][rack]["interfaces"])
  vxlan(cfg["leaves_mh"][rack]["vlan"],
        cfg["leaves_mh"][rack]["leaves"][device_name]["lo"],
        cfg["leaves_mh"][rack]["vrf"]),
  bonding(cfg["leaves_mh"][rack]["vlan"],
          cfg["leaves_mh"][rack]["leaves"][device_name]["ip"],
          cfg["leaves_mh"][rack]["bonds"],
          cfg["leaves_mh"][rack]["vrf"])
  mh(cfg["leaves_mh"][rack]["interfaces"],
     cfg["leaves_mh"][rack]["bonds"],
     cfg["leaves_mh"][rack]["mac"],
     cfg["leaves_mh"][rack]["leaves"][device_name]["prio"])

def set_hostdevice_name(hostdevice_name):
  msg = "nv set system hostname " + str(hostdevice_name) 
  print (msg)
  return msg

def mh(interfaces,bonds,mac,prio):
  if type(interfaces) == list:
    for interface in interfaces:
      print (f"nv set interface {interface} evpn multihoming uplink on")
  else:
    print (f"nv set interface {interfaces} evpn multihoming uplink on")
  for bond in bonds:
    print (f"nv set interface {bond} evpn multihoming segment local-id {bonds[bond]['id']}")
    print (f"nv set interface {bond} evpn multihoming segment mac-address {mac}")
    print (f"nv set interface {bond} evpn multihoming segment df-preference {prio}")

def mlag(eth0,mac,peerlink,anycast):
  msg = "nv set mlag backup " + str(eth0) + " vrf mgmt" + "\n"
  msg = msg + "nv set mlag mac-address " + str(mac) + "\n"
  msg = msg + "nv set interface peerlink bond member " + str(peerlink) + "\n"
  msg = msg + "nv set mlag peer-ip linklocal" + "\n"
  msg = msg + "nv set mlag init-delay 2" + "\n"
  msg = msg + "nv set nve vxlan mlag shared-address " + str(anycast) + "\n"
  msg = msg + "nv set system global anycast-mac " + str(mac)
  print(msg)
  return msg

def bgp(lo,asn,interfaces):
  msg = "nv set interface lo ip address " + str(lo) +"/32" + "\n"
  msg = msg + "nv set router bgp router-id " + str(lo) + "\n"
  msg = msg + "nv set router bgp autonomous-system " + str(asn) + "\n"
  msg = msg + "nv set vrf default router bgp address-family ipv4-unicast redistribute connected" + "\n"
  msg = msg + "nv set vrf default router bgp path-selection multipath aspath-ignore on" + "\n"
  if type(interfaces) == list:
    for interface in interfaces:
      msg = msg + "nv set vrf default router bgp neighbor " + str(interface) + " remote-as external" + "\n"
      msg = msg + "nv set vrf default router bgp neighbor " + str(interface) + " address-family l2vpn-evpn"
  else:
    msg = msg + "nv set vrf default router bgp neighbor " + str(interfaces) + " remote-as external" + "\n"
    msg = msg + "nv set vrf default router bgp neighbor " + str(interfaces) + " address-family l2vpn-evpn"
  msg = msg + "\n"
  print(msg)
  return msg

def vxlan(vlan,lo,vrf):
  msg = ""
  for vlan_id in vlan:
    msg = msg + "nv set bridge domain br_default vlan " + str(vlan_id) + " vni auto" + "\n"
  msg = msg + "nv set evpn enable on" + "\n"
  msg = msg + "nv set nve vxlan source address " + str(lo) + "\n"
  for tenant in vrf:
    msg = msg + "nv set vrf " + str(tenant) + " evpn vni " + str(vrf[tenant]['l3vni']) + "\n"
  print(msg)
  return msg

def bonding(vlan,ip,bonds,vrfs):
  msg = "nv set interface swp1-3,swp51 link state up" + "\n"
  for bond in bonds:
    msg = msg + "nv set interface " + str(bond) + " bond member " + str(bonds[bond]['int']) + "\n"
  for vlan_id in vlan:
      msg = msg + "nv set interface " + str(vlan[vlan_id]['access']) + " bond mlag id " + str(vlan[vlan_id]['access'][-1]) + "\n"
      msg = msg + "nv set interface " + str(vlan[vlan_id]['access']) + " bridge domain br_default access " + str(vlan_id) + "\n"
      msg = msg + "nv set interface vlan" + str(vlan_id) + " vlan " + str(vlan_id) + "\n"
      msg = msg + "nv set interface vlan" + str(vlan_id) + " ip address "+ str(vlan[vlan_id]['sub']) + "." + str(ip) + "/24" + "\n"
      msg = msg + "nv set interface vlan" + str(vlan_id) + " ip vrr address " + str(vlan[vlan_id]['sub']) + ".1/24" + "\n"
      for vrf in vrfs:
        msg = msg + "nv set interface vlan" + str(vlan_id) + " ip vrf " + str(vrf) + "\n"
  print(msg)
  return msg

############### Entrypoint ############### 
parser = argparse.ArgumentParser()
parser.add_argument("-d", "--Device", help = "Specify a device")
parser.add_argument("-c", "--Config", help = "Specify a config file")
parser.add_argument("-w", help="Export",action="store_true")
parser.add_argument("-a", help="Create topology in AIR",action="store_true")
parser.add_argument("-r", help="Reset",action="store_true")
args = parser.parse_args()

if not args.Config:
  print("Specify a config file '-c'")
  exit()

with open(str(args.Config), "r") as ymlfile:
    cfg = yaml.load(ymlfile,Loader=yaml.FullLoader)

def search_nested(name,flavor):
  for rack in cfg[flavor]:
    for leaf in cfg[flavor][rack]["leaves"]:
      # print(f"test sur leaf: {leaf}")
      if leaf == name:
        # print(f"found!")
        return rack

############### Display a single device ############### 
if args.Device:
    # print("Displaying Output as: % s" % args.Device)
    device_name = args.Device

    if device_name in cfg["spines"]:
      gen_spine(device_name)
    elif search_nested(device_name,"leaves_mlag") is not None:
      gen_leaf_mlag(device_name,search_nested(device_name,"leaves_mlag"))
    elif search_nested(device_name,"leaves_mh") is not None:
      gen_leaf_mh(device_name,search_nested(device_name,"leaves_mh"))
    elif device_name in cfg["borders"]:
      gen_border(device_name)
    else:
      print(f"Unable to parse {device_name} as a single device")
    exit()


############### Display the full environement  ############### 
if "spines" in cfg:
  print("##### SPINE #####")
  for spine in cfg["spines"]:
    print("=====================================================================")
    gen_spine(spine)
  
if "leaves_mlag" in cfg:
  print("##### MLAG #####")
  for rack in cfg["leaves_mlag"]:
    for leaf in cfg["leaves_mlag"][rack]["leaves"]:
      print("=====================================================================")
      gen_leaf_mlag(leaf,rack)
  
if "borders" in cfg:
  print("##### BORDER #####")
  for border in cfg["borders"]:
    print("=====================================================================")
    gen_border(border)

if "leaves_mh" in cfg:
  print("##### MH #####")
  for rack in cfg["leaves_mh"]:
    for leaf in cfg["leaves_mh"][rack]["leaves"]:
      print("=====================================================================")
      gen_leaf_mh(leaf,rack)


###############   ############### 
if args.a:
  trigger_air()

### EOF
